export const formatArrayOfPrimitives = (data) => {
  return {
    columns: null,
    data: data.map((item) => [item])
  }
}