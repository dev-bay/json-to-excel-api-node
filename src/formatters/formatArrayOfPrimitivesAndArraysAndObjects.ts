import * as Hlp from '../helpers';
import { formatArrayOfObjects } from './formatArrayOfObjects';

export const formatArrayOfPrimitivesAndArraysAndObjects = (data) => {
  const middleData = data.map((item) => typeof item === 'object' ? item : [item]);

  return formatArrayOfObjects(middleData);
}