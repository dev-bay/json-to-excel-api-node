import * as Hlp from '../helpers';

/**
 * PART: formatArrayOfObjects
 * [{}, {}, {}]
 */

const pathDivider = " J2E" + String((new Date()).getTime()) + "J2E ";

const removePathDivider = (path) => {
  return path.split(pathDivider).join(" -> ");
};

const createColName = (name, parentName?) => {
  return parentName ? parentName + pathDivider + name : name;
}

const getNestedNameParent = (nestedColumnNames) => {
  if(nestedColumnNames.length && nestedColumnNames[0]) {
    let nestedNameSplited = nestedColumnNames[0].split(pathDivider);
    nestedNameSplited.splice(nestedNameSplited.length - 1, 1);
    
    return nestedNameSplited.join(pathDivider);
  }

  return null;
}

const getNestedNameParentPath = (nestedName) => {
  let nestedNameSplited = nestedName.split(pathDivider);
  nestedNameSplited.splice(nestedNameSplited.length - 1, 1);

  return nestedNameSplited.join(pathDivider);
}

const getColumnsNames = (data, parentColName?) => {

  let columns = Object.keys(data[0]); // keys from first object in array

  if(parentColName) {
    columns = columns.map(( name ) => createColName(name, parentColName));
  }
  
  data.forEach((item, i) => {
    Object.keys(item).forEach(( innerItem ) => {
      const fullInnerItemName = createColName(innerItem, parentColName);

      if(columns.indexOf( fullInnerItemName ) === -1) {
        columns.push( fullInnerItemName );
      }

      if(Hlp.isObjNotArr(item[innerItem])) {

        let nestedColumnNames = getColumnsNames([item[innerItem]], innerItem);

        if(parentColName) {
          nestedColumnNames = nestedColumnNames.map(( name ) => createColName(name, parentColName));
        }

        nestedColumnNames = nestedColumnNames.filter(( nestedColumnName ) => {
          return columns.indexOf( nestedColumnName ) === -1;
        });

        if(columns.indexOf(fullInnerItemName) !== -1) {
          // Insert nested path from further object just after prev elment from the same path
          let inxToInsert = null
          let nestedNameParent = getNestedNameParent(nestedColumnNames);
          
          if(nestedNameParent) {
            
            inxToInsert = columns.reverse().findIndex((col) => {
              return col.indexOf(nestedNameParent) !== -1
            });

            inxToInsert = inxToInsert !== -1 ? columns.length - 1 - inxToInsert : null;
            
            columns.reverse();

          }

          if(inxToInsert) { //if there is relaed parent path from prev el then add elements just after that and delete parent element
            columns.splice(
              inxToInsert,
              0,
              ...nestedColumnNames
            );
            columns.splice(columns.indexOf(fullInnerItemName), 1);
          } else {
            columns.splice(
              columns.indexOf(fullInnerItemName),
              1,
              ...nestedColumnNames
            );
          }
          
        } else {
          columns.push(...nestedColumnNames);
        }

      }
      
    });
  });

  return columns;

}

const findPrevPathOccurency = (parentPath, currentInx, arr) => {
  const newArr = JSON.parse(JSON.stringify(arr));

  newArr.splice(currentInx, arr.length - currentInx);
  
  const parentPathPrevOccurencyReversedInx = newArr.reverse().findIndex((el) => {
      return el.indexOf(parentPath) !== -1;
  });
  
  const parentPathPrevOccurencyInx = (parentPathPrevOccurencyReversedInx !== -1) && (parentPathPrevOccurencyReversedInx !== currentInx) ? newArr.length - parentPathPrevOccurencyReversedInx : -1;

  return parentPathPrevOccurencyInx !== currentInx ? parentPathPrevOccurencyInx : -1;

}

const traverseParentPathUp = (el, inx, res, currentFullPath) => {
  if(el.indexOf(pathDivider) !== -1) {
      let nestedNameParent = getNestedNameParentPath(el);

      let prevPathOccurency = findPrevPathOccurency(nestedNameParent, inx, res);

      if(prevPathOccurency === -1) {
          traverseParentPathUp(nestedNameParent, inx, res, currentFullPath);
      } else {
          let currentInx = res.indexOf(currentFullPath);
          res.splice(currentInx, 1);
          res.splice(prevPathOccurency, 0, currentFullPath);
      }
  }
}

const groupArray = (arr) => {
  let res = JSON.parse(JSON.stringify(arr));

  arr.forEach((el, inx) => {
      var currentFullPath = el;
      traverseParentPathUp(el, inx, res, currentFullPath);
  });
  
  return res;
}

const takeObjDataByPath = (obj, path) => {
  const arr = path.split(pathDivider);
  let innerObj = obj;

  arr.forEach((item, i) => {
      if(i === 0) {
          innerObj = typeof obj[item] !== 'undefined' ? obj[item] : "";
      } else {
          innerObj = typeof innerObj[item] !== 'undefined' ? innerObj[item] : "";
      }
  });

  return innerObj;
}

export const formatArrayOfObjects = (data) => {
  
  let columns = getColumnsNames(data);

  columns = groupArray(columns);
  
  const resData = data.map((item) => {
    return columns.reduce((prev, curr)=> { // return new object based on headers with values from data array
      var value: any = "";

      if(curr.indexOf(pathDivider) !== -1) {
        value = takeObjDataByPath(item, curr);
      } else {
        value = item[curr];
      }
      
      if(typeof value === "object" && !Hlp.isNull(value)) {//value is array or object
        if(Hlp.isObjNotArr(value)) {
          
          value = Object.keys(value).map((key) => {
            return `${key}: ${value[key]}`;
          }).join(", ");

        } else if(Hlp.isArr(value)) {
          
          const { hasArrays, hasObjects } = Hlp.getArrayElTypes(value); // array types
          if(hasArrays || hasObjects) {
            value = value.map((item) => JSON.stringify(item)).join(", ")
          } else {
            value = value.join(", ");
          }

        } else {
          value = JSON.stringify(value);
        }
      } else { //value is not object or array
        value = typeof value !== "undefined" && !Hlp.isNull(value) ? String(value) : value;
      }

      prev.push(value);
      return prev;
    },[]);
  });
  
  return {
    columns: columns.map(removePathDivider),
    data: resData
  };

}