import * as Hlp from '../helpers';
import { formatArrayOfObjects } from './formatArrayOfObjects';

export const formatArrayOfPrimitivesAndObjects = (data) => {
  const middleData = data.map((item) => Hlp.isObjNotArr(item) ? item : [item]);

  return formatArrayOfObjects(middleData);
}