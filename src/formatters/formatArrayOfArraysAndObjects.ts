import { formatArrayOfObjects } from './formatArrayOfObjects';

export const formatArrayOfArraysAndObjects = (data) => {
  return formatArrayOfObjects(data);
}