import * as Hlp from '../helpers';

export const formatArrayOfPrimitivesAndArrays = (data) => {
  return {
    columns: null,
    data: data.map((item) => {
      return Hlp.isArr(item) ? item : [item];
    })
  }
}