import * as qs from 'qs';

/**
 * Function checks if provided data is Object (not array)
 * @param data - any kind of variable
 */

export const isNull = (val) => (!val && typeof val === "object");
export const isObjNotArr = (data) => !Array.isArray(data) && ( typeof data === "object") && !isNull(data);
export const isArr = (data) => Array.isArray(data) && ( typeof data === "object") && !isNull(data);

export const getArrayElTypes = (data) => {
  const hasPrymitives = data.findIndex((item) => {
    return typeof item !== 'object';
  });

  const hasArrays = data.findIndex((item) => {
    return isArr(item);
  });
    
  const hasObjects = data.findIndex((item) => {
    return isObjNotArr(item);
  });

  return {
    hasPrymitives: hasPrymitives !== -1,
    hasArrays: hasArrays !== -1,
    hasObjects: hasObjects !== -1,
  }

}

export const countObjectElTypes = (data) => {
  const arrFromObj = Object.keys(data).map((key) => data[key]);

  const prymitives = arrFromObj.filter((item) => {
    return typeof item !== 'object';
  });

  const arrays = arrFromObj.filter((item) => {
    return isArr(item);
  });
    
  const objects = arrFromObj.filter((item) => {
    return isObjNotArr(item);
  });

  return {
    countPrymitives: prymitives.length,
    countArrays: arrays.length,
    countObjects: objects.length
  }
}

const removeCharactersFromString = (characters, string) => {
  characters.forEach((character) => {
      string = string.split(character).join("")
  });

  return string;
}

export const formatSheetName = (name) => {
  return removeCharactersFromString([":", "/", "\\", "?", "*", "[", "]"], name).substring(0,30);
  
}

export const createUrlEncodedData = (data) => {
  return qs.stringify(data);
}


const appKeyCheck = (num) => {
  return num === 3218328439;
}

const appKeyCheckDay = (num) => {
  let day = (new Date()).getUTCDate();
  if (day * 444444232 === num) {
    return day;
  } else if ((day + 1) * 444444232 === num) {
    return day + 1;
  } else if ((day - 1) * 444444232 === num) {
    return day - 1;
  } else {
    return null;
  }
}

const appKeyCheckAlg = (checkDayNum, num) => {
  const day = appKeyCheckDay(checkDayNum);

  if(day === null) {
    return false;
  }

  const d = new Date();
  const zsz = d.getUTCFullYear();
  const rkr = day;
  const klk = d.getUTCMonth() + 1;
  
  return Math.abs(Math.ceil((zsz * 223) * Math.pow(klk, 6) / Math.cos(rkr))) * klk === num;
}

export const appKeyCheckAll = (uploadSizeAscii, uploadSizeHex, appKey) => {
  return appKeyCheck(appKey) && appKeyCheckAlg(uploadSizeHex, uploadSizeAscii);
}