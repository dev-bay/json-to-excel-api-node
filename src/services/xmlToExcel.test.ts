import * as fs from 'fs';
import * as path from 'path';
import axios from 'axios';
import { formatJsonFromPHP } from './xmlToExcel';
import { identifyAndPrepareData } from './jsonToExcel';
import { createUrlEncodedData } from '../helpers';

const testPath = path.join(__dirname, '../..', 'test-cases/xml') + '\\';
const names = [1,2,3,4,5,6,7,8,9,10,11,12,13,14];

names.forEach((name) => {
  test(`test case: ${name}`, async () => {
    const data_example_xml = fs.readFileSync(testPath + name + '.xml', {encoding: 'utf-8'});
    
    let { data: data_example_json } = await axios({
      method: 'post',
      headers: { 'content-type': 'application/x-www-form-urlencoded' },
      url: "http://localhost:8080/dev-apps/converter/xml/json-from-xml",
      data: createUrlEncodedData({
        data: data_example_xml
      })
    });

    data_example_json = formatJsonFromPHP( data_example_json );

    const data_example_res = JSON.parse(fs.readFileSync(testPath + name + '_res.json', {encoding: 'utf-8'}));
    const data = JSON.parse(JSON.stringify(identifyAndPrepareData(data_example_json)));
    
    expect(data).toStrictEqual(data_example_res);
  });
});


