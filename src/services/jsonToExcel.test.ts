import * as fs from 'fs';
import * as path from 'path';
import { identifyAndPrepareData } from './jsonToExcel';

const testPath = path.join(__dirname, '../..', 'test-cases/json') + '\\';
const names = [1,2,3,4,5,6,7,8,9,10,11,12,13,14];

names.forEach((name) => {
  test(`test case: ${name}`, () => {
    const data_example = JSON.parse(fs.readFileSync(testPath + name + '.json', {encoding: 'utf-8'}));
    const data_example_res = JSON.parse(fs.readFileSync(testPath + name + '_res.json', {encoding: 'utf-8'}));
    
    const data = JSON.parse(JSON.stringify(identifyAndPrepareData(data_example)));
    
    expect(data).toStrictEqual(data_example_res);
  });
});


