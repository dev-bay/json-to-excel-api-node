import * as Hlp from '../helpers';

export const formatJsonFromPHP = (data) => { //all changes done by reference!
  if (Hlp.isObjNotArr(data)) {
    Object.keys(data).forEach((key) => {
      
      formatObj(data, key);

    });
  } else if (Hlp.isArr(data)) {
    data.forEach((item, key) => {

      formatObj(data, key);

    })
  }

  return data;
}

function formatObj (data, key) {
  if (isObjForCleaning(data[key])) {
    data[key] = "";
  } else if (isObjToClearAttr(data[key])) {
    delete data[key]["@attributes"];
    formatObj(data, key);
  } else {
    formatJsonFromPHP(data[key]);
  }
}

function isObjForCleaning (obj) {
  const keys = Object.keys(obj);
  return Hlp.isObjNotArr(obj) && keys.length === 0;
}

function isObjToClearAttr (obj) {
  const keys = Object.keys(obj);
  return Hlp.isObjNotArr(obj) && keys.indexOf("@attributes") !== -1;
}
