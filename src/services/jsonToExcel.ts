import * as Hlp from '../helpers'
import * as IDF from '../formatters/_inputDataFormatters';

const prepareObjData = (data) => {
  let res: any = undefined;
  const { countPrymitives, countArrays, countObjects } = Hlp.countObjectElTypes(data);

  if(countPrymitives === 0 && (countArrays + countObjects > 0)) { // 1. tylko obiekty lub tablice -> sheets i w pojedynczym sheet weryfikacja jak tutaj od nowa
    
    res = { sheets: {} };

    Object.keys(data).forEach((key) => {
      const sheetName = Hlp.formatSheetName(key);
      const innerData = Hlp.isArr(data[key]) ? data[key] : [data[key]];
      res.sheets[sheetName] = prepareArrayData(innerData);
    });

  } else if(countPrymitives > 0 && (countArrays + countObjects === 0)) { // 2. Tylko prymitywy -> opakowac w array i wyswietlac standardowo lub w sposob powielajacy tablice w dół (jak na tablicy na dole po srodku napisalem)
    
    res = prepareArrayData([data]);

  } else if(countPrymitives > 0 && (countArrays + countObjects > 0)) {
    // 3. prymitywy i obiekty i/lub tablice:
    //    3a. primitives.length > complex.length -> opakowac caly 1 obiekt w array i jak w pkt 2
    //    3a. primitives.length < complex.length -> sheets
    
    
    if(countArrays + countObjects >= countPrymitives) {
      res = { sheets: {} };
      Object.keys(data).forEach((key) => {
        const sheetName = Hlp.formatSheetName(key);
        const innerData = Hlp.isArr(data[key]) ? data[key] : [data[key]];
        res.sheets[sheetName] = prepareArrayData(innerData);
      });
    } else {
      res = prepareArrayData([data]);
    }

  }

  return res;
}

export const prepareArrayData = (data) => {
  const { hasPrymitives, hasArrays, hasObjects } = Hlp.getArrayElTypes(data); // array types

  if (hasPrymitives && !hasArrays && !hasObjects) { // tylko prymitywy

    return IDF.formatArrayOfPrimitives(data);

  } else if(!hasPrymitives && !hasArrays && hasObjects) { // tylko obiekty - standardowa sytuacja
    
    return IDF.formatArrayOfObjects(data);

  } else if(!hasPrymitives && hasArrays && !hasObjects) { // tylko tablice
    
    return IDF.formatArrayOfArrays(data);

  } else if(hasPrymitives && hasArrays && !hasObjects) { // prymitywy i tablice
    
    return IDF.formatArrayOfPrimitivesAndArrays(data);

  } else if(hasPrymitives && !hasArrays && hasObjects) { // prymitywy i obiekty
    
    return IDF.formatArrayOfPrimitivesAndObjects(data);

  } else if(!hasPrymitives && hasArrays && hasObjects) { // tablice i obiekty - jak na tablicy.
    
    return IDF.formatArrayOfArraysAndObjects(data);

  } else if(hasPrymitives && hasArrays && hasObjects) { // prymitywy, tablice i obiekty - ale jakos to trzeba pokryc - prymityw pakowac w array i tyle
    
    return  IDF.formatArrayOfPrimitivesAndArraysAndObjects(data);

  }
}

/**
 * 
 * @param data 
 * @param isDeeperLevel - first or next entry into this function. In next entry sheets creation is not possible!
 */

export const identifyAndPrepareData = (data) => {
  let res: any = undefined;

  if(typeof data === "object" && !Hlp.isNull(data)) {
    if(Hlp.isObjNotArr(data)) { // object on the begining - create sheets // co zawiera w sobie obiekt?

      res = prepareObjData(data);
      
    } else { //array
      
      res = prepareArrayData(data);

    }
  } else { //data is one single primitive value like string or number
    res = IDF.formatArrayOfPrimitives([data]);
  }

  return res;
}


