import * as srvJsonToExcel from './jsonToExcel';
import * as Hlp from '../helpers';
import * as IDF from '../formatters/_inputDataFormatters';

export const flatify = (data) => {
  // TU WAŻNE!!! TO NIE MA BYĆ TRAKTOWANE JAKO SHEETS!!! TYLKO ZAWSZE JAKO OBIEKTY!!!
  // I TO W prepareObjData MA ODPALAĆ DRUGIEGO IFA ZAWSZE!!!
  // albo po prostu to odpalac z marszu prepareArrayData([data]);
  let resData;

  if(typeof data === "object" && !Hlp.isNull(data)) {
    if(Hlp.isObjNotArr(data)) {
      resData = srvJsonToExcel.prepareArrayData([data]);
    } else {
      resData = srvJsonToExcel.prepareArrayData(data);
    }
    
  } else { //data is one single primitive value like string or number
    resData = IDF.formatArrayOfPrimitives([data]);
  }

  if(Hlp.isObjNotArr(resData) && resData.sheets && Hlp.isObjNotArr(resData.sheets)) {
    
    return Object.keys(resData.sheets).reduce((prevVal, currVal) => {
      prevVal[currVal] = flatifyHeadersWithData(resData.sheets[currVal])
      return prevVal;
    }, {});

  } else if(Hlp.isObjNotArr(resData) && resData.data && Hlp.isArr(resData.data)) {
    return flatifyHeadersWithData(resData);
  } else {
    return "ERROR! INVALID JSON!"
  }
};

const flatifyHeadersWithData = (data) => {
  if(data.columns && Hlp.isArr(data.columns) && data.columns.length && data.data && Hlp.isArr(data.data) && data.data.length) {

    return data.data.map((item) => {
      return item.reduce((prevVal, currVal, i) => {
        let key = data.columns[i] ? data.columns[i] : i;
        prevVal[key] = currVal;
        return prevVal;
      }, {});
    });

  } else if(Hlp.isNull(data.columns) && data.data && Hlp.isArr(data.data) && data.data.length) {
    if(data.data.length === 1) {

      if (Hlp.isArr(data.data[0]) && data.data[0].length && data.data[0].length === 1) {
        return data.data[0][0];
      } else if (Hlp.isArr(data.data[0]) && data.data[0].length) {
        return takeSingleItemFromArray(data.data[0]);
      } else {
        return takeSingleItemFromArray(data.data);
      }

    } else {
      return takeSingleItemFromArray(data.data);
    }
  } else {
    return "ERROR! INVALID JSON!"
  }

  
};

const takeSingleItemFromArray = (arr) => {
  return arr.map((item) => {
    return Hlp.isArr(item) && item.length === 1 ? item[0] : item;
  });
}