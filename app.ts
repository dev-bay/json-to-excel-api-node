import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as fs from 'fs';
import * as path from 'path';
import * as morgan from 'morgan';
import * as slowDown from 'express-slow-down';
import * as rateLimit from 'express-rate-limit';
import * as srvJsonToExcel from './src/services/jsonToExcel';
import * as srvXmlToExcel from './src/services/xmlToExcel';
import * as srvFlatify from './src/services/jsonFlatify';

import { appKeyCheckAll } from './src/helpers';

const app = express();
const PORT = process.env.PORT || 3010;
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })

const speedLimiter = slowDown({
  windowMs: 15 * 60 * 1000, // 15 minutes
  delayAfter: 100, // allow 100 requests per 15 minutes, then...
  delayMs: 500 // begin adding 500ms of delay per request above 100:
});

const apiLimiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100
});

app.enable("trust proxy"); //FOR HEROKU DEPLOY ONLY!!!

app.use(morgan(':remote-addr :remote-user :method :url HTTP/:http-version :status :res[content-length] - :response-time ms [:date[clf]]', { stream: accessLogStream }));
app.use(speedLimiter);
app.use("/", apiLimiter);
app.use(bodyParser.json({limit: '2mb'}));

app.get("/", (req, res, next) => {
  res.send("PUBLIC API");
  next();
});

app.use((err, req, res, next) => {
  if (err) {
    console.log("error");
    res.status(400).send('error parsing data')
  } else {
    console.log("next possible step");
    next();
  }
  
});

app.listen(PORT);

app.use((req, res, next) => {
  if(req.headers?.app_key === 'b51d316c-032b-408b-96f3-ad6280a7664b') {
    console.log("run");
    next();
  } else {
    console.log("401");
    res.sendStatus(401);
  }
  
});

app.post('/validate-keys', async (req, res, next) => {
  const { body: data } = req;
  
  try {
    console.log(data);
    const check = appKeyCheckAll(data.uploadSizeAscii, data.uploadSizeHex, data.appKey);
    if(check) {
      res.send("ok");
    } else {
      res.sendStatus(401);
    }
  } catch (err) {
    res.sendStatus(401);
  }
  
  
  next();
});

app.post('/format-json-to-excel', async (req, res, next) => {
  const { body: data } = req;
  console.log(data);
  try {
    let dataToSend = srvJsonToExcel.identifyAndPrepareData(data);
    console.log(dataToSend);
    res.send(dataToSend);
  } catch (err) {
    res.send("wrong input");
  }
  
  
  next();
});

app.post('/format-json-from-xml', async (req, res, next) => {
  const { body: data } = req;

  try {
    let dataToSend = srvXmlToExcel.formatJsonFromPHP(data);
    res.send(dataToSend);
  } catch (err) {
    res.send("wrong input");
  }
  
  next();
});

app.post('/json-flatify', async (req, res, next) => {
  const { body: data } = req;

  try {
    let dataToSend = srvFlatify.flatify(data);
    if(typeof dataToSend === 'number') {
      res.send('' + dataToSend);
    } else {
      res.send(dataToSend);
    }
    
  } catch (err) {
    res.send("wrong input");
  }
  
  next();
});

console.log("server works on port " + PORT);